var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');
let mongoClient = require('mongodb').MongoClient;
let ObjectId = require('mongodb').ObjectId;
var cors = require('cors');
require('dotenv').config({path: __dirname + '/../.env'})

const APP_PORT = process.env['APP_PORT'] || 3000;

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
    type ArticleListResponse {
        page: Int
        totalPage: Int
        limit: Int
        totalData: Int
        message: String
        data: [Article]
    }
    type Article {
        _id: String
        title: String
        contents: String
        createdAt: String
        comments: [Comments]
    }
    type Comments {
        _id: String
        comment: String
        articleId: String
        createdAt: String
    }
    type Mutation {
        deleteArticle(id: String!): Boolean
        insertArticle(title: String!, contents: String!): Article
        updateArticle(id: String!, title: String!, contents: String!): Article
        insertComment(articleId: String!, comment: String!): Comments
        updateComment(id: String!, articleId: String!, comment: String!): Comments
        deleteComment(id: String!): Boolean
    }
    type Query {
       getArticleList(page: Int, limit: Int, sorting: String): ArticleListResponse
       getArticleById(id: String!): Article
    }
`);
 
// The root provides a resolver function for each API endpoint
var root = {
    deleteArticle: async (args) => {
        let id = args.id;
        
        let objId = new ObjectId(id);
        try {
            const db = await connect();
            const result = await db.collection('article').deleteOne({"_id": objId});
            
            return true;
        } catch(e) {
            console.log(e);
            return false;
        }
    },
    insertArticle: async (args) => {
        let title = args.title;
        let contents = args.contents;
        let data = {
            title,
            contents,
            createdAt: new Date()
        }
        try {
            const db = await connect();
            const result = await db.collection('article').insertOne(data);
            data._id = result.insertedId;
            return data;
        } catch(e) {
            console.log(e);
            return null;
        }
    },
    updateArticle: async (args) => {
        let title = args.title;
        let contents = args.contents;
        let id = args.id;
        let data = {
            title,
            contents
        }
        let objId = new ObjectId(id); 
        try {
            const db = await connect();
            
            let existedData = await db.collection('article').findOne({"_id": objId});
            if(existedData !== null) {
                existedData.title = title;
                existedData.contents = contents;
                db.collection('article').updateOne({"_id": objId}, {$set: existedData}, {
                    upsert: false
                })

                return existedData;
            } else {
                const result = await db.collection('article').insertOne(data);
                data._id = result.insertedId;
                return data;
            }
        } catch(e) {
            console.log(e);
            return null;
        }
    },
    insertComment: async (args) => {
        let articleId = new ObjectId(args.articleId);
        //let objId = new ObjectId(id);
        try {
            const db = await connect();
            let data = {
                articleId: articleId,
                comment: args.comment,
                createdAt: new Date()
            }
            const result = await db.collection('comment').insertOne(data);
            data._id = result.insertedId;
            return data;
        } catch(e) {
            console.log(e);
            return null;
        }
    },
    updateComment: async (args) => {
        let comment = args.comment;
        let id = args.id;
        let data = {
            comment
        }
        let objId = new ObjectId(id); 
        try {
            const db = await connect();
            
            let existedData = await db.collection('comment').findOne({"_id": objId});
            if(existedData !== null) {
                existedData.comment = comment;
                db.collection('comment').updateOne({"_id": objId}, {$set: existedData}, {
                    upsert: false
                })


                return existedData;
            } else {
                const result = await db.collection('article').insertOne(data);
                data._id = result.insertedId;
                return data;
            }
        } catch(e) {
            console.log(e);
            return null;
        }
    },
    deleteComment: async (args) => {
        let id = args.id;
        
        let objId = new ObjectId(id);
        try {
            const db = await connect();
            const result = await db.collection('comment').deleteOne({"_id": objId});
            
            return true;
        } catch(e) {
            console.log(e);
            return false;
        }
    },
    getArticleList: async (args) => {
        let response = {
            page: 1,
            totalPage: 1,
            limit: 1,
            message: '',
            totalData: 1,
            data: []
        };

        let limit = args.limit;
        let page = args.page;
        let offset = limit * (page - 1);
        let sort = 1;
        if(args.sorting.toLowerCase() == 'desc') {
            sort = -1;
        }

        try {
            const db = await connect();
            let totalData = await db.collection('article').count();
            if(page > 0) {
                let data = await db.collection('article').find({}, {sort: {"createdAt": sort}})
                        .skip(offset).limit(limit).toArray();
                let totalPage = Math.ceil(totalData / limit);
                response.page = page;
                response.totalPage = totalPage;
                response.limit = limit;
                response.totalData = totalData;
                response.data = data;
            } else {
                let data = await db.collection('article').find({}, {sort: {"createdAt": 1}})
                        .toArray();
                
                response.totalData = totalData;
                response.data = data;
            }
            return response;
        } catch(e) {
            console.log(e);
            response.message = e.message;
            return response;
        }
    },
    getArticleById: async (args) => {
        let id = new ObjectId(args.id);

        try {
            const db = await connect();
            let article = await db.collection('article').findOne({"_id": id});
            let comments = await db.collection('comment').find({"articleId": article._id})
                    .toArray();
            if(comments !== null) {
                article.comments = comments;
            }
            return article;
        } catch(e) {
            console.log(e);
            return false;
        }
    }
};
 
var app = express();
app.use(cors());
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));

app.listen(APP_PORT);
console.log(`Running a GraphQL API server at http://localhost:${APP_PORT}/graphql`);

const connect = async () => {
    const username = process.env['MONGO_USERNAME'] || '';
    const password = process.env['MONGO_PASSWORD'] || '';
    const cluster = process.env['MONGO_CLUSTER'] || '';
    const database = process.env['MONGO_DB'] || '';
    let url = `mongodb+srv://${username}:${password}@${cluster}/${database}?retryWrites=true&w=majority`;

    const client = await mongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true});

    return client.db(database);
}