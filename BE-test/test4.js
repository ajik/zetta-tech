/**
 * Direction:
 * Find missing number from the list
 *
 * Expected Result:
 * 8
 */
const numbers = [9, 6, 4, 2, 3, 5, 7, 0, 1];

function result(numbers) {
  // Your Code Here
  numbers.sort();
  let n = numbers[0];
  for(let i of numbers) {
    if(i !== n) {
      return n;
    }
    n += 1;
  }
  return null;
}

console.log(result(numbers));
