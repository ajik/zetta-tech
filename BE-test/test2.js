/**
 * Direction:
 * Remove duplicated data from array
 * 
 * Expected Result:
 * [1, 2, 3, 4, 5]
 */
const data = [1, 4, 2, 3, 5, 3, 2, 4];

function result(data) {
  // Your Code Here
  let mapData = {};
  let res = [];
  for(let n of data) {
    if(n in mapData) {} else {
      mapData[n] = n;
      res.push(n);
    }
  }
  return res;
}

console.log(result(data));
