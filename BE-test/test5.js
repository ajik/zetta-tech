/**
 * Direction:
 * Find prefix of the word from array of string
 *
 * Expected Result:
 * fl
 */
const words = ['flower', 'flow', 'flight'];

function result(words) {
  // Your Code Here
  let res = "";
  words.sort(function(a, b) {
    if(a.length < b.length) {
      return -1;
    } else if(a.length > b.length) {
      return 1;
    } else {
      return 0;
    }
  });
  let prefix = "";
  for(let i in words) {
    if(i == 0) {
      prefix = words[i];
    } else {
      let prefixTemp = "";
      for(let j = 0; j < words[i].length; j++) {
        if(j <= prefix.length) {
          if(prefix.charAt(j) == words[i].charAt(j)) {
            prefixTemp += words[i].charAt(j);
          }
        } else {
          break;
        }
      }
      prefix = prefixTemp;
    }
  }
  return prefix;
}

console.log(result(words));
