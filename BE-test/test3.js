/**
 * Direction:
 * Remove key that have null or undefined value
 *
 * Expected Result:
 * [
 *   { session_name: 'first test', classes: [{ students: [{ student_name: 'budi' }] }] },
 *   { classes: [{ class_name: 'second class', students: [{ student_name: 'adi' }] }] },
 * ]
 */
const data = [
  { session_name: 'first test', classes: [{ class_name: undefined, students: [{ student_name: 'budi' }] }] },
  { session_name: null, classes: [{ class_name: 'second class', students: [{ student_name: 'adi' }] }] },
];

function result(data) {
  // Your Code Here
  data = deleteNUllOrUndefined(data);
  return data;
}

function deleteNUllOrUndefined(data) {
  let restData;
  for(let key in data) {
    if(data[key] === undefined || data[key] === null) {
      delete data[key];
    } else if(typeof data[key] === "object" || Array.isArray(data[key])) {
      data[key] = deleteNUllOrUndefined(data[key]);
    }
  }
  return data;
}

console.log(JSON.stringify(result(data)));
